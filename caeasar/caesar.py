import argparse

def test():
  def testSanitize():
    x = Caesar()
    c1="ABC"
    assert (x.sanitize('abc') == c1)
    assert (x.sanitize('Abc') == c1)
    assert (x.sanitize('A b c') == c1)
    assert (x.sanitize('A%b2c') == c1)
  def testEncrypt():
    x = Caesar()
    c1="ILIKECHEESE"
    p2="COMTE"
    result="KZUDIEVQXWG"
    assert (x.encrypt(p2,c1) == result)
  def testDecrypt():
    x = Caesar()
    c1="ILIKECHEESE"
    p2="COMTE"
    result="KZUDIEVQXWG"
    assert (x.decrypt(p2,result) == c1)
  testSanitize()
  testEncrypt()
  testDecrypt()

def ForeverIterate(foo):
  while True:
    for x in foo:
      yield x

class Caesar(object):
  def __init__(self):
    self._encryptDict =dict()
    self._decryptDict=dict()
    for x1 in range (26):
      c1=chr(ord('A')+x1)
      self._encryptDict[c1]=dict()
      for x2 in range(26):
        c2=chr(ord('A')+x2)
        c3=chr(ord('A')+((x1+x2) % 26))
        self._encryptDict[c1][c2]=c3
        try:
          self._decryptDict[c3][c2]=c1
        except KeyError:
          self._decryptDict[c3] = dict()
          self._decryptDict[c3][c2]=c1
  def sanitize (self,dirty):
    return ''.join([x for x in dirty.upper() if x.isalpha()])
  def encrypt(self, password, clear):
    return self._techTransform(password, clear, self._encryptDict)
  def decrypt(self, password, encrypted):
    return self._techTransform(password, encrypted, self._decryptDict)
  def _techTransform(self, password, clear, convertTable):
    p1 = self.sanitize (password)
    c1 = self.sanitize(clear)
    c2=list()
    for p,x in zip(ForeverIterate(p1),iter(c1)):
      c2.append(convertTable[x][p])
    return ''.join(c2)


def main():
  cipher = Caesar()
  parser = argparse.ArgumentParser(description='Encrypt/Decrypt using caesar cipher.')
  parser.add_argument('--password',
                      help='password used')
  parser.add_argument("--decrypt",dest="action", action="store_const",
                      const=cipher.decrypt,default=cipher.encrypt,
                      help="Use decrypt instead of defualt encrypt")
  parser.add_argument("text",nargs="+",help="text to encrypt or decrypt",)
  args = parser.parse_args()

  print args.action(args.password,''.join(args.text))

if __name__ == '__main__':
  test()
  main()
